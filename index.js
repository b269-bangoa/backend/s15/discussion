// [Section] Sntax statements and comments
// Single Line /**/ Multi-line

// Statements-instructions we tell the computer to perform
console.log("Hello World");

// Whitespace - computer doesn't read it
console   . log (  "Hello World")

// Syntax -set of rules that describes how statements must be constructed

// [SECTION] Variables- used to contain data

/*Declaring variables
	let/const variableName;
*/

let myVariables = 0;

console.log(myVariables);

/* Variable must be declared first before they are used:

console.log(hello);
let hello;

RULES:
1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
2. Variable names should start with a lowercase character, use camelCase for multiple words.
3. For constant variables, use the 'const' keyword.
4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
*/

// Declaring & Initializing

let productName = 'desktop computer';
console.log(productName);

let productPrice = 19000;
console.log(productPrice)

const interest = 3.539;
console.log(interest);

let productPrice2 = productPrice*interest;
console.log(productPrice2);

productPrice = productPrice2;

productPrice = productPrice2*interest;

console.log(productPrice,productPrice2);


//  var (JS 1997) let/const (JS 2015)
// [Difference between var & let/const] 

// 1. Hoisting is JS default behavior of moving initialization to the top - var can hoist declaration
a = 5;
console.log(a);
var a;

// 2. Scope - let/const cannot access variables inside a block
let outerVariable = 'hello';
{
	let innerVariable = 'hello again'
}

innerVariable = outerVariable;

console.log(outerVariable,innerVariable);

// Multiple Declaration
let productCode = '12123', productBrand = 'Dell';
console.log(productCode,productBrand);

// Using a variable using reserved word -not possible (eg. let)


// [SECTION] Data Types
// String
let country = 'Philippines';
let province = "Metro Manila";

// Concatenating Strings
let fullAddress = province+','+country;
console.log(fullAddress);
console.log(province,',',country);

// Escape Character (\)
let mailAddress = 'Metro Manila\nPhilippines';
console.log(mailAddress);


let message = 'John\'s employees went home early.';
console.log(message);

// Numbers -integers, decimal,fractions,exp
let headCount = 26;
let grade = 98.7;
let planetDistance = 2e10;
console.log(headCount,grade,planetDistance);

// Combine text and number
console.log("John's grade last quarter is "+grade);

// Boolean
let isMarried = false;
let inGoodConduct = true;

console.log("isMarried: " +isMarried,"inGoodConduct "+inGoodConduct);

// Arrays - store multiple values
/*
Syntax
let/const arrayName = [elementA,elementB,...]
*/

let grades = [98.7,92.1,90.2,94.6];
console.log(grades);

// Objects
/*
Syntax 
let/const objectName = {propertyA:valueA,propertyB:valueB,...}
*/

let person = {
	fullName: 'John', 
	age: 35,
	isMarried: false,
	contact: [123,456,789],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
}

console.log(person);

let myGrades = {
	firstGrading: 98.8,
	secondGrading: 90.8
};

console.log(myGrades);

// typeof operator -check what data type
console.log(typeof myGrades,typeof grades);

// Null and Undefined
let spouse = null;
console.log(spouse);


